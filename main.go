package main

import (
	"log"
	"net/http"
	"os"

	_ "github.com/lib/pq"
)

func (a *Application) Run(addr string) {
	log.Fatal(http.ListenAndServe(":8000", a.Router))
}

func main() {
	a := Application{}
	a.Initialize(
		os.Getenv("APP_DB_HOST"),
		os.Getenv("APP_DB_PORT"),
		os.Getenv("APP_DB_USERNAME"),
		os.Getenv("APP_DB_PASSWORD"),
		os.Getenv("APP_DB_NAME"),
		os.Getenv("APP_MG_SERVER"),
		os.Getenv("APP_MG_DATABASE"))

	a.Run(":8080")
}
