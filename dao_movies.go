package main

import (
	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type Movie struct {
	ID          bson.ObjectId `bson:"_id" json:"id"`
	Name        string        `bson:"name" json:"name"`
	CoverImage  string        `bson:"cover_image" json:"cover_image"`
	Description string        `bson:"description" json:"description"`
}

func (movie *Movie) findById(mgo *mgo.Database, id string) error {
	error := mgo.C(COLLECTION).FindId(bson.ObjectIdHex(id)).One(&movie)
	return error
}

func (movie *Movie) insert(mgo *mgo.Database) error {
	error := mgo.C(COLLECTION).Insert(&movie)
	return error
}

func (movie *Movie) delete(mgo *mgo.Database) error {
	error := mgo.C(COLLECTION).Remove(&movie)
	return error
}

func (movie *Movie) update(mgo *mgo.Database) error {
	error := mgo.C(COLLECTION).UpdateId(movie.ID, &movie)
	return error
}

func (movie *Movie) findAll(mgo *mgo.Database) ([]Movie, error) {
	var movies []Movie
	error := mgo.C(COLLECTION).Find(bson.M{}).All(&movies)
	return movies, error
}
