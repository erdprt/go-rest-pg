package main

import (
	"fmt"
	"net/http"
	"os"
)

func ping(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(w, "Application running")
}

// @TODO: update to return json response
func (a *Application) healthCheck(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(w, "Application running")

	environmentString :=
		fmt.Sprintf("database settings: host=%s port=%s dbname=%s user=%s password=%s ", os.Getenv("APP_DB_HOST"), os.Getenv("APP_DB_PORT"), os.Getenv("APP_DB_NAME"), os.Getenv("APP_DB_USERNAME"), os.Getenv("APP_DB_PASSWORD"))
	fmt.Fprintln(w, environmentString)

	current_timestamp := checkPostgresql(a.DB)
	databaseCheckString := fmt.Sprintf("database check: currentTimeStamp=%s ", current_timestamp)
	fmt.Fprintln(w, databaseCheckString)
}
