package main

import (
	"database/sql"
	"encoding/json"
	"net/http"

	"github.com/gorilla/mux"
	"gopkg.in/mgo.v2/bson"
)

func (a *Application) findAllMovies(w http.ResponseWriter, r *http.Request) {
	var movie Movie
	movies, err := movie.findAll(a.MGO)
	if err != nil {
		respondMovieWithError(w, http.StatusInternalServerError, err.Error())
		return
	}
	respondMovieWithJson(w, http.StatusOK, movies)
}

func (a *Application) findMovie(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id := vars["id"]

	var movie Movie
	if err := movie.findById(a.MGO, id); err != nil {
		switch err {
		case sql.ErrNoRows:
			respondMovieWithError(w, http.StatusNotFound, "Product not found")
		default:
			respondMovieWithError(w, http.StatusInternalServerError, err.Error())
		}
		return
	}
	respondMovieWithJson(w, http.StatusOK, movie)
}

func (a *Application) createMovie(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()
	var movie Movie
	if err := json.NewDecoder(r.Body).Decode(&movie); err != nil {
		respondMovieWithError(w, http.StatusBadRequest, "Invalid request payload")
		return
	}
	movie.ID = bson.NewObjectId()
	if err := movie.insert(a.MGO); err != nil {
		respondMovieWithError(w, http.StatusInternalServerError, err.Error())
		return
	}
	respondMovieWithJson(w, http.StatusCreated, movie)
}

func (a *Application) updateMovie(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()
	var movie Movie
	if err := json.NewDecoder(r.Body).Decode(&movie); err != nil {
		respondMovieWithError(w, http.StatusBadRequest, "Invalid request payload")
		return
	}
	if err := movie.update(a.MGO); err != nil {
		respondMovieWithError(w, http.StatusInternalServerError, err.Error())
		return
	}
	respondMovieWithJson(w, http.StatusOK, map[string]string{"result": "success"})
}

func (a *Application) deleteMovie(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()
	var movie Movie
	if err := json.NewDecoder(r.Body).Decode(&movie); err != nil {
		respondWithError(w, http.StatusBadRequest, "Invalid request payload")
		return
	}
	if err := movie.delete(a.MGO); err != nil {
		respondMovieWithError(w, http.StatusInternalServerError, err.Error())
		return
	}
	respondMovieWithJson(w, http.StatusOK, map[string]string{"result": "success"})
}

func respondMovieWithError(w http.ResponseWriter, code int, msg string) {
	respondMovieWithJson(w, code, map[string]string{"error": msg})
}

func respondMovieWithJson(w http.ResponseWriter, code int, payload interface{}) {
	response, _ := json.Marshal(payload)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	w.Write(response)
}
